"""
@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
"""

import unittest
from capture_nom import capture_nom_fichier


class Test(unittest.TestCase):

    def test_capture_nom(self):

        entree = "2017.03.12_45_dsc7848.jpg"
        attendu = ("2017.03.12_45_", "dsc7848", ".jpg")
        self.assertEqual(capture_nom_fichier(entree), attendu)

        entree = "2017.03.12_45_dsc7849.jpg"
        attendu = ("2017.03.12_45_", "dsc7849", ".jpg")
        self.assertEqual(capture_nom_fichier(entree), attendu)

        entree = "2017.03.12_45_mat.JPG"
        attendu = ("2017.03.12_45_", "mat", ".JPG")
        self.assertEqual(capture_nom_fichier(entree), attendu)

        entree = "2017.03.12_dsc7848.mpeg"
        attendu = ("2017.03.12_", "dsc7848", ".mpeg")
        self.assertEqual(capture_nom_fichier(entree), attendu)

        entree = "2017.03.12_dsc7848.mpeg"
        attendu = ("2017.03.12_", "dsc7848", ".mpeg")
        self.assertEqual(capture_nom_fichier(entree), attendu)

        entree = "gggg.jpg"
        attendu = "gggg.jpg"
        self.assertEqual(capture_nom_fichier(entree), attendu)

        entree = "10"
        attendu = "10"
        self.assertEqual(capture_nom_fichier(entree), attendu)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_capture_nom']
    unittest.main()
