# -*-coding:utf8;-*-
"""
@author: jingl3s

"""

# license
#
# Producer 2017 jingl3s
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import json
import sys
from common.logger_config import LoggerConfig
import os
from common.configuration_loader import ConfigurationLoader
import re
from prepare_renomme import Renomme
from pathlib import Path

dossier_config = os.path.join(os.path.dirname(__file__), "configs")


def lance_programme():
    """ """
    if 1 >= len(sys.argv):
        print("Aucun fichier fourni")
        exit(1)
    else:
        list_fichier = sys.argv[1:]

    logger, config_json = configure(config_select="config_text.json")

    logger.debug(config_json)

    list_utilisateur = config_json["DEFAUT"][:]
    list_utilisateur.append("===> Texte à fournir")
    index_saisie = len(list_utilisateur) - 1
    config_text = config_json["TEXTE"][:]
    list_utilisateur += config_json["TEXTE"][:]

    # Question utilisateur
    list_affichage = ["{}: {}".format(i, j) for i, j in enumerate(list_utilisateur)]
    message = "\r\n".join(list_affichage)
    choix = input(message + "\r\nChoix options (séparateur espace ou ,)>")
    logger.debug(choix)
    list_choix = re.split(r"(\d+)", choix)
    list_text = list()
    for item in list_choix:
        if item.isdigit():
            if int(item) < len(list_utilisateur):
                if int(item) != index_saisie:
                    list_text.append(list_utilisateur[int(item)])
                else:
                    if int(item) == index_saisie:
                        texte_utilisateur = input("fournir texte : ")
                        if texte_utilisateur != "":
                            list_text.append(texte_utilisateur)
                            if texte_utilisateur not in config_text:
                                config_text.append(texte_utilisateur)
                                config_text = config_text[-4:]
    # PREPARATION noms fichiers sortie
    logger.debug(list_text)
    ren = Renomme()
    ren.set_separateur("_")
    ren.set_cle_debut_supprime(config_json["CLE_SUPPR"])
    sort = ren.prepare_list(list_fichier, list_text)

    if 0 < len(sort):
        # demande confirmation avant de renommer
        content = ""
        for fic in sort:
            content = content + "\r\n{}".format(fic)

        logger.info("Texte remplacement: {}".format("_".join(list_text)))
        logger.info("Prevision remplacement: {}".format(content))

        texte_utilisateur = input("Voulez vous valider [vide annule]")

        # Renommage fichiers
        if texte_utilisateur != "":
            ren.renomme()
            if config_text != config_json["TEXTE"]:
                config_json["TEXTE"] = config_text
                serialized = json.dumps(config_json)
                (Path(dossier_config) / Path("config_text.json")).write_text(serialized)
        else:
            logger.info("Annulé")
    else:
        logger.error("Aucun fichiers à renommer")


def configure(config_select=None):
    """
    :param config_select:

    :return: tuple avec en premier un logger configuré et en second la configuration json lues

    """

    log_config = "INFO"
    # CONFIG
    _config_json = None
    if config_select is not None:
        file = config_select
        gestion_config = ConfigurationLoader(dossier_config)
        gestion_config.set_configuration_file_name(file)
        _config_json = gestion_config.obtenir_configuration()
        if "LOGGER" in _config_json:
            log_config = _config_json["LOGGER"]

    # LOGGER
    my_logger = LoggerConfig(
        os.path.join(os.path.dirname(__file__), "log"), "lanceur", str(log_config)
    )
    _logger = my_logger.get_logger()

    _logger.debug("LOGGER activated: {}".format(log_config))

    _logger.debug(_config_json)

    return _logger, _config_json


if __name__ == "__main__":
    lance_programme()
